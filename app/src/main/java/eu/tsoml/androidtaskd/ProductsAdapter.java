package eu.tsoml.androidtaskd;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductViewHolder> {

    List<Product> products;
    ProductsAdapterClickListener listener;

    ProductsAdapter(List<Product> products) {
        this.products = products;
    }

    void setListener(ProductsAdapterClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item, parent, false);
        return new ProductViewHolder(v, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        Product product = products.get(position);
        holder.bind(product);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView productTitleTextView;

        ProductViewHolder(@NonNull View itemView, ProductsAdapterClickListener listener) {
            super(itemView);
            productTitleTextView = itemView.findViewById(R.id.productTitleTextView);
        }

        void bind(final Product product) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClicked(product);
                }
            });
            productTitleTextView.setText(product.getName());

        }
    }
}
