package eu.tsoml.androidtaskd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Generator {
    static List<Product> generate() {
        ArrayList<Product> list = new ArrayList<>(Arrays.asList(
                new Product("Google Pixel 4", 800, ProductType.Phone),
                new Product("OnePlus 8 Pro", 750, ProductType.Phone),
                new Product("IPad Pro", 1000, ProductType.Tablet)));
        return list;
    }
}
