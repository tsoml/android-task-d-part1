package eu.tsoml.androidtaskd;

public interface ProductsAdapterClickListener {
    void onClicked(Product product);
}
