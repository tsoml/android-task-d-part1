package eu.tsoml.androidtaskd;

class Product {

    private String name;
    private double price;
    private ProductType type;

    Product(String name, double price, ProductType type) {
        this.name = name;
        this.price = price;
        this.type = type;
    }

    String getName() {
        return name;
    }

    double getPrice() {
        return price;
    }

    ProductType getType() {
        return type;
    }
}
