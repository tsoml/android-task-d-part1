package eu.tsoml.androidtaskd;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import eu.tsoml.androidtaskd.databinding.FragmentProductListBinding;


public class ProductListFragment extends Fragment {

    private FragmentProductListBinding binding;
    private ProductsAdapter productsAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentProductListBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        productsAdapter = new ProductsAdapter(Generator.generate());

        productsAdapter.setListener(new ProductsAdapterClickListener() {
            @Override
            public void onClicked(Product product) {
                AlertDialog dialog = new AlertDialog.Builder(getActivity())
                        .setTitle(product.getName())
                        .setMessage("Price: " + product.getPrice() + "\nType: " + product.getType())
                        .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create();
                dialog.show();
            }
        });


        binding.recyclerView.setAdapter(productsAdapter);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
